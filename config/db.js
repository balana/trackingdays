// var configValues = require('./config');

const mongoose = require('mongoose');

mongoose.connect('mongodb://mongo:27017/trackingdays').then( () => {
    console.log('App connected to db with success')})
    .catch( (e) => {
        console.error(e)
    });

mongoose.Promise = global.Promise;

//Get the default connection
const db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = db;

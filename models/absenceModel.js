const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const AbsenceSchema = new Schema({
    id: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    startDate : {
         type: Date,
         default: Date.now,
         required: [true, 'Start date type is required']
    },
    endDate : {
        type: Date,
        required: [true, 'End date type is required']
    },
    absenceType: {
        type: String,
        required: [true, 'Absence type is required'],
        default: 'holiday'
    }
});

const Absence = mongoose.model('Absences', AbsenceSchema);

module.exports = Absence;
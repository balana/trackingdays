const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    firstName : {
        type: String,
        required: [true, 'First name is required']
    },
    lastName : {
        type: String,
        required: [true, 'Last name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
const AbsenceModel = require('../models/absenceModel');
// const UserModel = require('../models/userModel');
const holidayTypes = ["holiday", "halfHoliday", "homeOffice", "halfHomeOffice", "sickLeave", "all"];
const randomstring = require("randomstring");
const utils = require('./utils');

const fs = require('fs');
const pdf = require('html-pdf');
const options = { format: 'Letter' };


exports.get_holiday_by_type = function (req, res) {

    const userId = req.params.userId;
    const absenceType = req.query.type;

    utils.checkIfUserExists(userId, (err, isUserFound) => {
        if (err) {
            utils.sendInternalStatusError(err, res);
            return;
        }
        if (isUserFound) {

            if (!validateHolidayType(absenceType, res)) {
                return;
            }
            let query = {userId: userId};
            if (absenceType != 'all') {
                query.type = absenceType;
            }
            AbsenceModel.find(query).select().exec()
                .then(function (result) {
                    console.log(result);
                    res.send(result);
                })
                .catch(function (err) {
                    console.log(err);
                });
        } else {
            res.status(404);
            res.send(`Cannot retrieve absence because User id ${userId} does not exist`);
            return;
        }
    });
};

exports.get_all_absences_of_user_report = function (req, response) {

    const userId = req.params.userId;
    const contentDisposition = req.query.inline ? "inline" : "attachment";

    utils.checkIfUserExists(userId, (err, isUserFound, user) => {

        if (err) {
            utils.sendInternalStatusError(err, response);
            return;
        }
        if (isUserFound) {
            let query = {userId: userId};
            const userFound = user[0];
            AbsenceModel.find(query).select('absenceType startDate endDate').exec()
                .then(function (result) {
                    let absences = [];
                    for (var i in result) {
                        absences.push( `<li><p>${result[i].absenceType}</p><p>from: ${result[i].startDate}</p><p>to: ${result[i].endDate}</p></li>`);
                    }
                    let absenceList = absences.join(' ');
                    var mockHTML = `
                        <html><body><h2>Report of absences for ${userFound.firstName} ${userFound.lastName}</h2>
                        <p><b>Absences:</b> ${absenceList}</p>
                        </body></html>`;

                    pdf.create(mockHTML, options).toStream( (err, stream) => {
                        if(err) {
                            console.log("Something wrong happened");
                            utils.sendInternalStatusError(err, response);
                        }
                        response.setHeader('Content-Disposition', `${contentDisposition}; filename=report.pdf`);
                        response.setHeader('Content-Type', 'application/pdf');
                        stream.pipe(response);
                    });
                })
                .catch(function (err) {
                    console.log(err);
                });
        } else {
            response.status(404);
            response.send(`Cannot retrieve absence because User id ${userId} does not exist`);
            return;
        }
    });
};

exports.get_holiday_by_id = function (req, res) {

};

exports.save_holiday = function (req, res) {
    const userId = req.params.userId;
    // validate the absence type
    if (!validateHolidayType(req.body.type, res)) {
        return;
    }
    // validate that user exists
    utils.checkIfUserExists(userId, (err, isUserFound) => {
        if (err) {
            utils.sendInternalStatusError(err, res);
            return;
        }
        if (isUserFound) {
            let absence = new AbsenceModel({
                userId: req.params.userId,
                id: randomstring.generate(5),
                startDate: new Date(req.body.startDate),
                endDate: new Date(req.body.endDate),
                absenceType: req.body.type
            });

            absence.save().then(function (results) {
                console.log('Saved absence to database');
                res.status(201);
                res.send(results);
            }).catch(function (err) {
                console.log(err);
            });

        } else {
            res.status(404);
            res.send(`Cannot save absence because User id ${userId} does not exist. Create user first`);
        }
    });
};

exports.delete_holiday = function (req, res) {
    const userId = req.params.userId;
    const absenceId = req.params.absenceId;

    utils.checkIfUserExists(userId, (err, isUserFound) => {
        if (err) {
            utils.sendInternalStatusError(err, res);
            return;
        }
        if (isUserFound) {
            console.log("1");
            AbsenceModel.findOneAndRemove({userId: userId, id: absenceId}).exec()
                .then(function (result) {
                    if (!result) {
                        res.status(404);
                        res.send(`User with id: ${userId} and absence id: ${absenceId} was not found`);
                        return;
                    }
                    return result.remove();
                })
                .then(function (results) {
                    res.send(results)
                })
                .catch(function (err) {
                    console.log(err);
                });
        } else {
            res.status(404);
            res.send(`Cannot delete absence because User id ${userId} does not exist.`);
        }
    });
};

function validateHolidayType(absenceType, res) {
    if (!absenceType) {
        res.status = 400;
        res.send('Missing absence type.');
        return false;
    } else if (holidayTypes.indexOf(absenceType) == -1) {
        res.status = 400;
        res.send('This absence type is not supported');
        return false;
    }
    return true;
}
const db = require('../config/db');
const UserModel = require('../models/userModel');

// check if specific collection is sent
exports.checkIfCollectionExists = function checkIfCollectionExists(collectionName, callback) {
    db.db.listCollections({name: collectionName})
        .next(function (err, collinfo) {
            if(err) {
                callback(err);
            }
            if (!collinfo) {
                console.log(`Collection ${collectionName} does not exist`);
                callback(null, false);
            }
            else {
                console.log(`Collection ${collectionName} exists`);
                callback(null, true);
            }
        });
};

// check if user exists before, calling callback function with variable true or false
exports.checkIfUserExists = function checkIfUserExists(userId, callback) {
    UserModel.find({userId: userId}).select()
        .exec()
        .then(function (doc) {
            if (doc && doc.length>0) {
                callback(null, true, doc);
            }
            else {
                callback(null, false);
            }
        })
        .catch(function (err) {
            console.log(err);
            callback(err);
        });
};

exports.sendInternalStatusError = function sendInternalStatusError(err, res) {
    res.status(503);
    res.send('ERROR: ' + err.toString());
};
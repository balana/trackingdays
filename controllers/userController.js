const db = require('../config/db');
const UserModel = require('../models/userModel');
const AbsenceModel = require('../models/absenceModel');
const randomstring = require("randomstring");
const loadas = require('lodash');
const utils = require('./utils');

const usersCollectionName = 'users';

exports.get_users = function (req, res) {

    utils.checkIfCollectionExists(usersCollectionName, (err, result) => {
        if(err) {
            utils.sendInternalStatusError(err, res);
            return;
        }
        if (result) {
            UserModel.find().select().exec()
                .then(function (result) {
                    res.send(result);
                }).catch(function (err) {
                console.log("ERR: " + err);
            });
        }
        else {
            res.send([]);
        }
    });
};

function saveUser(user, res) {
    user.save().then(function (results) {
        console.log(`Saved user ${user.firstName} to database`);
        res.status(201);
        res.send(results);
    }).catch(function (err) {
        console.log(err);
        res.status(400);
        res.send("User was not saved");
    });
}

exports.save_user = function (req, res) {

    let user = new UserModel({
        userId: randomstring.generate(5),
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email
    });
    // check if collection exists
    utils.checkIfCollectionExists(usersCollectionName, (err, result) => {
        if(err) {
            utils.sendInternalStatusError(err, res);
            return;
        }
        if(result){
            // collection exists
            UserModel.find({email: user.email}).select()
                .exec()
                .then(function (usersArray) {
                    // if user already found on db, is not created again
                    let userFound = loadas.find(usersArray, { email: user.email} );
                    if (!userFound) {
                        saveUser(user, res);
                    } else {
                        console.log(`User ${user.email} exists already in the database`);
                        res.status(200);
                        res.send(userFound);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                });
        } else {
            // collection does not exist
            saveUser(user, res);
        }
    });
};

exports.get_user = function (req, res) {
    const id = req.params.userId;

    UserModel.findOne({userId:id}).select('firstName lastName email')
        .exec()
        .then(function (user) {
            if (user) {
                    res.send(user);
                } else {
                    res.status(404);
                    res.send(`User with ${id} was not found in db`);
                }
            }).catch(function (err) {
                console.log(err);
        });
};

exports.delete_user = function (req, res) {
    const id = req.params.userId;

    AbsenceModel.find({userId: id}).select('userId absenceType').exec()
        .then(function (absence) {
            if (absence.length > 0) {
                res.status(403);
                res.send("There are absences on this user, cannot delete.");
            } else if (absence.length < 1) {
                UserModel.findOneAndRemove({userId: id}).select().exec()
                    .then(function (user) {
                        if (user) {
                            let msg = `User ${user.lastName} with id: ${user.userId} was deleted`;
                            console.log(msg);
                            res.status(204);
                            res.send(msg);
                        } else {
                            let msg = "User was not found";
                            console.log(msg);
                            // could send 204 status code (no content); Same for delete above
                            res.status(404);
                            res.send(msg);
                        }
                    }).catch(function (err) {
                    console.log(err);
                });
            }
        }).catch(function (err) {
        console.log(err);
    });
};
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var db = require('./config/db');
const bodyParser = require('body-parser');


// var setupController = require('./controllers/holidayController');

var port = process.env.PORT || 3000;

//connect to mongo db
app.use(bodyParser.json());
// initialise routes
// app.use('/v1', require('./routes/absence'));
app.use('/v1', require('./routes/user'));


// mongoose.connect(db);
console.log("App started");
// setupController(app);


app.listen(port);

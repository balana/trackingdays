const express = require('express');
const router = express.Router();
const user = require('../controllers/userController');
const absence = require('../controllers/absenceController');


router.get('/users',  user.get_users);
router.post('/users',  user.save_user);
router.get('/users/:userId',  user.get_user);
router.delete('/users/:userId',  user.delete_user);

//all absences of one user
router.get('/users/:userId/absences/report',  absence.get_all_absences_of_user_report);
//all absences of one user by type
router.get('/users/:userId/absences',  absence.get_holiday_by_type);
router.post('/users/:userId/absences', absence.save_holiday);
router.delete('/users/:userId/absences/:absenceId',  absence.delete_holiday);
// TODO
router.get('/users/:userId/absences/:absenceId',  absence.get_holiday_by_id);

module.exports = router;
